<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HotelControllerTest extends WebTestCase
{
    public function testCollection()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'hotels');
    }

    public function testCreate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'hotels');
    }

    public function testGet()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'hotel/{id}');
    }

    public function testUpdate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'hotels/{id}');
    }

    public function testDelete()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'hotels/{id}');
    }

}
