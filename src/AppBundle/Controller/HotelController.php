<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HotelController extends Controller
{
    /**
     * @Rest\Get("hotels")
     */
    public function collectionAction()
    {
        $data = ['hello' => 'world'];
        return $data;
    }

    /**
     * @Rest\Post("hotels")
     */
    public function createAction()
    {
        return 'this is post';
    }

    /**
     * @Rest\Get("hotels/{id}")
     */
    public function getAction($id)
    {
        // get hotel
        $hotel = 'some hotel';

        return $hotel;
    }

    /**
     * @Rest\Post("hotels/{id}")
     */
    public function updateAction($id)
    {
        return 'hotel updated';
    }

    /**
     * @Rest\Delete("hotels/{id}")
     */
    public function deleteAction($id)
    {
        return 'hotel deleted';
    }

}
